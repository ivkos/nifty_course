import numpy as np
from matplotlib import pyplot as plt

last_year = np.loadtxt('LastYear.txt')
print(last_year[0,...].sum())
print(last_year[1,...].sum())
total_error = np.square(last_year[2,...])
total_error = total_error.sum()
total_error = np.sqrt(total_error)
print(total_error)

plt.plot(last_year[1,...])
plt.plot(last_year[0,...])
plt.show()