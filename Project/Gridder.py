import numpy as np
from matplotlib import pyplot as plt
import math

continentalUS = np.loadtxt('continentalUS.txt')
continentalUS = np.around(continentalUS)
continentalUS =  continentalUS.astype(int)*2

minimum_latitude = np.amin(continentalUS[:,0])

maximum_latitude = np.amax(continentalUS[:,0])
minimum_longitude = np.amin(continentalUS[:,1])
maximum_longitude = np.amax(continentalUS[:,1])

longitude_range = maximum_longitude-minimum_longitude
latitude_range = maximum_latitude-minimum_latitude
grid = np.zeros((latitude_range, longitude_range))

for i in range(continentalUS.shape[0]):
    grid[continentalUS[i,0]-minimum_latitude-1 ,continentalUS[i,1]-minimum_longitude-1] +=1

#np.savetxt('continentalUS_gridded.txt', grid)

ax = plt.subplot()
im = ax.imshow(np.arcsinh(grid))
plt.show()


    
