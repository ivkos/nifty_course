import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

#density = np.loadtxt('continentalUS_gridded.txt')
error = np.loadtxt('2D_std.txt')

#density = density/(density.sum())

fig, ax1 = plt.subplots(figsize=(20, 5), ncols=1)
image = ax1.imshow(np.log(error))
divider = make_axes_locatable(ax1)
cax1 = divider.append_axes("right", size="5%", pad=0.05)
fig.colorbar(image, cax=cax1)

# error = ax2.imshow(np.log(error))
# divider = make_axes_locatable(ax2)
# cax2 = divider.append_axes("right", size="5%", pad=0.05)
# fig.colorbar(error, cax=cax2)
plt.savefig('2D_std.png')
plt.show()