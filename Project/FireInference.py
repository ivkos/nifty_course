import numpy as np
from matplotlib import pyplot as plt
import nifty5 as ift


fire_number = np.loadtxt('months.txt')
fire_number = fire_number.astype(np.int)
summed = np.zeros(int(fire_number.shape[0]/12))
for i in range(int(fire_number.shape[0]/12)):
    summed[i] = fire_number[i*12:(i+1)*12].sum()

fire_number = summed
fire_number = fire_number.astype(np.int)


#mask = np.random.choice(2, fire_number.shape[0])
#mask = np.append(np.zeros(fire_number.shape[0]-12), np.ones(12))
mask = np.append(np.zeros(fire_number.shape[0]-1), np.ones(1))
#mask = np.append(mask, np.zeros(108))

position_space = ift.RGSpace(fire_number.shape)
mask = ift.from_global_data(position_space, mask)
R = ift.MaskOperator(mask)


harmonic_space = position_space.get_default_codomain()
HT = ift.HarmonicTransformOperator(harmonic_space, target=position_space)
power_space = ift.PowerSpace(harmonic_space)
A = ift.SLAmplitude(
    **{
        'target': power_space,
        'n_pix': 2,  # 64 spectral bins
        # Smoothness of spectrum
        'a': 10,  # relatively high variance of spectral curvature
        'k0': .2,  # quefrency mode below which cepstrum flattens
        # Power-law part of spectrum
        'sm': -3,  # preferred power-law slope
        'sv': .6,  # low variance of power-law slope
        'im': -2,  # y-intercept mean, in-/decrease for more/less contrast
        'iv': 2.  # y-intercept variance
    })

correlated_field = ift.CorrelatedField(position_space,A)
data =  ift.from_global_data(position_space, fire_number)
#R =  ift.GeometryRemover(position_space)
signal = ift.exp(correlated_field)

signal_response = R @ signal

likelihood = ift.PoissonianEnergy(R(data))
likelihood = likelihood @ signal_response

#Numerical stuff
ic_sampling = ift.GradientNormController(iteration_limit=100)
ic_newton = ift.GradInfNormController(name='Newton', tol=1e-6, iteration_limit=30)
minimizer = ift.NewtonCG(ic_newton)

#Solve inference problem 
ham = ift.StandardHamiltonian(likelihood, ic_sampling)
mean = ift.full(ham.domain, 0.)

# Draw five samples and minimize KL, iterate 10 times
for _ in range(15):
    KL = ift.MetricGaussianKL(mean, ham, 10)
    KL, _ = minimizer(KL)
    mean = KL.position

N_posterior_samples = 100
KL = ift.MetricGaussianKL(mean, ham, 100)
KL, _ = minimizer(KL)
mean = KL.position

sc = ift.StatCalculator()
sky_samples, pspec_samples = [], []
i=0
full = []
last_year = []
for sample in KL.samples:
    tmp = signal(sample + KL.position)
    sc.add(tmp)
    sky_samples.append(tmp)
    pspec_samples.append(A.force(sample)**2)
    if i%20==0:
        full.append(tmp.to_global_data())
        plt.plot(tmp.to_global_data())
    i += 1
plt.show()
plt.close()
full = np.asanyarray(full)
np.savetxt('posterior_samples.txt', full)
np.savetxt('posterior_samples_zoomed_in.txt', last_year)

result = sc.mean.to_global_data()
np.savetxt('summed_reconstruction.txt', result)
error = ift.sqrt(sc.var).to_global_data()
np.savetxt('summed_reconstruction_error.txt', error)
#last_year = np.zeros((3,12))
# for i in range(12):
#     last_year[0,i] = fire_number[fire_number.shape[0]-13+i]
#     last_year[1,i] = result[result.shape[0]-13+i]
#     last_year[2,i] = error[error.shape[0]-13+i]
# np.savetxt('LastYear.txt', last_year)

pspec_samples = pspec_samples[0].to_global_data()
plt.plot(pspec_samples)
plt.xscale('log')
plt.yscale('log')
plt.savefig('PowerSpectrum.png')
plt.show()
plt.close()

plt.plot(result)
plt.plot(fire_number)
plt.fill_between(np.arange(result.shape[0]), y1 = result + error, y2=result - error, alpha = 0.3)
plt.savefig('full_reconstruction.png')
plt.show()

# plt.plot(result[-24:])
# plt.plot(fire_number[-24:])
# plt.fill_between(np.arange(24), y1 = result[-24:] + error[-24:], y2=result[-24:] - error[-24:], alpha = 0.3)
# plt.savefig('zoomed_in.png')
# plt.show()