import numpy as np
from matplotlib import pyplot as plt
import nifty5 as ift
from helpers import (checkerboard_response, generate_gaussian_data,
                     plot_prior_samples_2d, plot_reconstruction_2d)

fire_locations = np.loadtxt('continentalUS_gridded.txt')

fire_locations = fire_locations.astype(np.int)

position_space = ift.RGSpace(fire_locations.shape)
#mask = ift.from_global_data(position_space, mask)
#R = ift.MaskOperator(mask)
R = ift.GeometryRemover(position_space)

harmonic_space = position_space.get_default_codomain()
HT = ift.HarmonicTransformOperator(harmonic_space, target=position_space)
power_space = ift.PowerSpace(harmonic_space)
A = ift.SLAmplitude(
    **{
        'target': power_space,
        'n_pix': 64,  # 64 spectral bins
        # Smoothness of spectrum
        'a': 10,  # relatively high variance of spectral curvature
        'k0': .2,  # quefrency mode below which cepstrum flattens
        # Power-law part of spectrum
        'sm': -5,  # preferred power-law slope
        'sv': 0.3,  # low variance of power-law slope
        'im': -2,  # y-intercept mean, in-/decrease for more/less contrast
        'iv': 2.  # y-intercept variance
    })

correlated_field = ift.CorrelatedField(position_space,A)
data =  ift.from_global_data(position_space, fire_locations)
#R =  ift.GeometryRemover(position_space)
signal = ift.exp(correlated_field)

signal_response = R @ signal

likelihood = ift.PoissonianEnergy(R(data))
likelihood = likelihood @ signal_response

#Numerical stuff
ic_sampling = ift.GradientNormController(iteration_limit=100)
ic_newton = ift.GradInfNormController(name='Newton', tol=1e-6, iteration_limit=30)
minimizer = ift.NewtonCG(ic_newton)

#Solve inference problem 
ham = ift.StandardHamiltonian(likelihood, ic_sampling)
initial_mean = 0.01*ift.from_random("normal", ham.domain)
initial_mean = ift.MultiField.from_dict(initial_mean)
initial_mean = ift.MultiField.full(ham.domain, 0.)
mean = initial_mean
#mean = ift.full(ham.domain, 0.)

# Draw five samples and minimize KL, iterate 10 times
for _ in range(15):
    KL = ift.MetricGaussianKL(mean, ham, 10)
    KL, _ = minimizer(KL)
    mean = KL.position

N_posterior_samples = 30
KL = ift.MetricGaussianKL(mean, ham, 30)
KL, _ = minimizer(KL)
mean = KL.position

sc = ift.StatCalculator()
sky_samples, pspec_samples = [], []
i=0
for sample in KL.samples:
    tmp = signal(sample + KL.position)
    sc.add(tmp)
    sky_samples.append(tmp)
    pspec_samples.append(A.force(sample)**2)


result = sc.mean.to_global_data()
np.savetxt('2D_reconstruction.txt', result)
error = ift.sqrt(sc.var).to_global_data()
np.savetxt('2D_std.txt',error)
pspec_samples = pspec_samples[0].to_global_data()
plt.plot(pspec_samples)
plt.xscale('log')
plt.yscale('log')
plt.savefig('PowerSpectrum2D.png')
plt.show()
plt.close()

ax = plt.subplot()
im = ax.imshow(np.log(result))
plt.savefig('2D_reconstruction.png')
plt.show()
plt.close()

ax = plt.subplot()
im = ax.imshow(np.log(error))
plt.savefig('2D_std.png')
plt.show()