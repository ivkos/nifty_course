from dbfread import DBF
import numpy as np
from matplotlib import pyplot as plt

table = DBF('wf_nps_1980_2016.dbf', encoding='UTF-8')
new_table=[]
for item in table:
    new_table.append(item)

#create coordinate array without the 274 elements which have no coordinates
coordinates = np.zeros((len(new_table)-274, 2))

item = 0
for i in range(len(new_table)-137):  
    if new_table[i]['DLATITUDE'] != 0:
        coordinates[item,0] = new_table[i]['DLATITUDE']
        coordinates[item,1] = new_table[i]['DLONGITUDE']
        item += 1

continentalUS = []
for coordinate in coordinates:
    if coordinate[1] > -130:
        continentalUS.append(coordinate)
continentalUS = np.asanyarray(continentalUS)
print(continentalUS.shape)
np.savetxt('continentalUS.txt', continentalUS)

plt.plot(continentalUS[:,1], continentalUS[:,0], 'bo') 
#plt.plot(coordinates[:,1], coordinates[:,0], 'bo') 
plt.savefig('continental_locations.png')
plt.show()

