import numpy as np
from matplotlib import pyplot as plt
import nifty5 as ift
from helpers import generate_wf_data, plot_WF, generate_mysterious_data

np.random.seed(42)

# Want to implement: m = Dj = (S^{-1} + R^T N^{-1} R)^{-1} R^T N^{-1} d


position_space = ift.RGSpace([48, 116])

prior_spectrum = lambda k: 1./(10. + k**(1.0))

data = np.loadtxt('continentalUS_gridded.txt')
print(np.amax(data))
print(np.average(data))
R = ift.GeometryRemover(position_space)
data_space = R.target
data = ift.from_global_data(data_space, data)

N = ift.ScalingOperator(0.1, data_space)

harmonic_space = position_space.get_default_codomain()
HT = ift.HartleyOperator(harmonic_space, target=position_space)

S_h = ift.create_power_operator(harmonic_space,  prior_spectrum)
S = ift.SandwichOperator.make(HT.adjoint, S_h)

D_inv = S.inverse + R.adjoint @ N.inverse @ R
j = (R.adjoint @ N.inverse)(data)

IC = ift. GradientNormController(iteration_limit = 100, tol_abs_gradnorm=1e-7)
D = ift.InversionEnabler(D_inv.inverse, IC, approximation = S)

m = D(j)
ground_truth2 = S.draw_sample()
D = ift.WienerFilterCurvature(R, N, S, IC, IC).inverse
N_samples = 10
samples = [D.draw_sample()+m for i in range(N_samples)]

m_np = m.to_global_data()
ax = plt.subplot()
im = ax.imshow(m_np)
plt.show()

