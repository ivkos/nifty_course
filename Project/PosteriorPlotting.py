import numpy as np
from matplotlib import pyplot as plt

data = np.loadtxt('posterior_samples.txt')
truth = np.loadtxt('months.txt')

for i in range(5):
    plt.plot(data[i,-24:])

plt.plot(truth[-24:], label='ground truth')
plt.legend()
plt.savefig('posterior_samples_zoomed.png')
plt.show()