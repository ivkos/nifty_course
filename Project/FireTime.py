from dbfread import DBF
import numpy as np
from matplotlib import pyplot as plt

table = DBF('wf_nps_1980_2016.dbf', encoding='UTF-8')
new_table=[]
for item in table:
    new_table.append(item)

dates = [] 
for element in new_table:
    dates.append(element['STARTDATED'])
dates.sort()

months = (dates[len(dates)-1].year-dates[0].year+1)*12
fire_numbers = np.zeros(months)

for date in dates:
    fire_numbers[(date.year-1980)*12+date.month-1] += 1

np.savetxt('months.txt', fire_numbers)
